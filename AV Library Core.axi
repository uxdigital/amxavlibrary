PROGRAM_NAME='AV Library Core'
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 09/24/2014  AT: 13:14:48        *)
(*******************************************************************************)
(*                                                                             *)
(*     _____            _              _  ____             _                   *)
(*    |     | ___  ___ | |_  ___  ___ | ||    \  ___  ___ |_| ___  ___  ___    *)
(*    |   --|| . ||   ||  _||  _|| . || ||  |  || -_||_ -|| || . ||   ||_ -|   *)
(*    |_____||___||_|_||_|  |_|  |___||_||____/ |___||___||_||_  ||_|_||___|   *)
(*                                                           |___|             *)
(*                      ___ _    __                                            *)
(*                     /   | |  / /                                            *)
(*                    / /| | | / /                                             *)
(*                   / ___ | |/ /                                              *)
(*                  /_/ _|_|__|/__                                             *)
(*                     / /   (_) /_  _________ ________  __                    *)
(*                    / /   / / __ \/ ___/ __ `/ ___/ / / /                    *)
(*                   / /___/ / /_/ / /  / /_/ / /  / /_/ /                     *)
(*                  /_____/_/_.___/_/   \__,_/_/   \__, /                      *)
(*                                                /____/                       *)
(*                                                                             *)
(*                   � Control Designs Software Ltd (2012)                     *)
(*                         www.controldesigns.co.uk                            *)
(*                                                                             *)
(*      Tel: +44 (0)1753 208 490     Email: support@controldesigns.co.uk       *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*                         AV Library (Test Build)                             *)
(*                                                                             *)
(*            Written by Mike Jobson (Control Designs Software Ltd)            *)
(*                                                                             *)
(** REVISION HISTORY ***********************************************************)
(*                                                                             *)
(*  Please see README.md included with this library                            *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*  Permission is hereby granted, free of charge, to any person obtaining a    *)
(*  copy of this software and associated documentation files (the "Software"), *)
(*  to deal in the Software without restriction, including without limitation  *)
(*  the rights to use, copy, modify, merge, publish, distribute, sublicense,   *)
(*  and/or sell copies of the Software, and to permit persons to whom the      *)
(*  Software is furnished to do so, subject to the following conditions:       *)
(*                                                                             *)
(*  The above copyright notice and this permission notice and header shall     *)
(*  be included in all copies or substantial portions of the Software.         *)
(*                                                                             *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *)
(*  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *)
(*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *)
(*  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *)
(*  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT  *)
(*  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR   *)
(*  THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                 *)
(*                                                                             *)
(*******************************************************************************)

DEFINE_CONSTANT

INTEGER AV_NAME_MAX_LEN = 50
INTEGER AV_KEY_MAX_LEN = 50
INTEGER AV_VAR_STR_MAX_LEN = 100
INTEGER AV_MAX_NUMBER_OF_VARS = 10

#IF_NOT_DEFINED AV_VAR_TYPE_INT
INTEGER AV_VAR_TYPE_INT = 1
#END_IF

#IF_NOT_DEFINED AV_VAR_TYPE_CHAR
INTEGER AV_VAR_TYPE_CHAR = 2
#END_IF

DEFINE_TYPE

STRUCT _VAR {
    CHAR key[AV_KEY_MAX_LEN]
    CHAR valueAsString[AV_VAR_STR_MAX_LEN]
    SINTEGER valueAsInt
    INTEGER type
}

STRUCT _AV_DEVICE {
    INTEGER defined
    INTEGER id
    INTEGER type
    CHAR key[AV_KEY_MAX_LEN]
    CHAR name[AV_NAME_MAX_LEN]
    _VAR var[AV_MAX_NUMBER_OF_VARS]
    CHAR linkedDeviceKey[AV_NAME_MAX_LEN]
}

DEFINE_FUNCTION INTEGER AVDeviceVarKeyIsReservedWord(CHAR keyName[]) {
    STACK_VAR CHAR lowerKey[AV_VAR_STR_MAX_LEN]
    lowerKey = LOWER_STRING(keyName)
    
    if(lowerKey == 'defined') return TRUE
    if(lowerKey == 'id') return TRUE
    if(lowerKey == 'type') return TRUE
    if(lowerKey == 'key') return TRUE
    if(lowerKey == 'name') return TRUE
    if(lowerKey == 'var') return TRUE
    if(lowerKey == 'linkedDeviceKey') return TRUE
    
    return FALSE
}

DEFINE_FUNCTION AVDeviceInit(_AV_DEVICE device) {
    STACK_VAR INTEGER varIndex
    
    device.defined = FALSE
    device.id = 0
    device.key = ''
    device.name = ''
    device.type = 0
    
    for(varIndex = 1; varIndex <= MAX_LENGTH_ARRAY(device.var); varIndex ++) {
	device.var[varIndex].key = ''
	device.var[varIndex].valueAsInt = 0
	device.var[varIndex].valueAsString = ''
	device.var[varIndex].type = 0
    }
}

DEFINE_FUNCTION AVDevicesInit(_AV_DEVICE devices[], INTEGER type) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	AVDeviceInit(devices[n])
	devices[n].type = type
	devices[n].id = n
    }
}

DEFINE_FUNCTION INTEGER AVDevicesCount(_AV_DEVICE devices[]) {
    STACK_VAR INTEGER n
    STACK_VAR INTEGER count
    
    count = 0
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	if(devices[n].defined) {
	    count ++
	}
    }
    
    return count
}

DEFINE_FUNCTION INTEGER AVDeviceIDForKey(_AV_DEVICE devices[], CHAR deviceKey[]) {
    STACK_VAR INTEGER n;
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	if(devices[n].key == deviceKey) {
	    return devices[n].id
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER AVDeviceIndexForID(_AV_DEVICE devices[], INTEGER id) {
    STACK_VAR INTEGER n;
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	if(devices[n].id == id) {
	    return n
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER AVDeviceIndexForKey(_AV_DEVICE devices[], CHAR deviceKey[]) {
    STACK_VAR INTEGER n;
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	if(devices[n].key == deviceKey) {
	    return n
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER AVDeviceSet(_AV_DEVICE devices[], CHAR deviceKey[], CHAR deviceName[]) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	if(devices[n].defined && devices[n].key == deviceKey) {
	    devices[n].name = deviceName
	    return devices[n].id
	}
    }
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(devices); n ++) {
	if(!devices[n].defined) {
	    devices[n].defined = TRUE
	    devices[n].key = deviceKey
	    devices[n].name = deviceName
	    return devices[n].id
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER AVDeviceVarIndexGet(_AV_DEVICE device, CHAR varKey[]) {
    STACK_VAR INTEGER varIndex
    
    if(!LENGTH_STRING(varKey)) return 0
    
    for(varIndex = 1; varIndex <= MAX_LENGTH_ARRAY(device.var); varIndex ++) {
	if(device.var[varIndex].key == varKey) {
	    return varIndex
	}
    }
}

DEFINE_FUNCTION INTEGER AVDeviceVarSetInt(_AV_DEVICE device, CHAR varKey[], SINTEGER value) {
    STACK_VAR INTEGER varIndex
    
    varIndex = AVDeviceVarIndexGet(device, varKey)
    
    if(AVDeviceVarKeyIsReservedWord(varKey)) {
	return 0
    }
    
    if(varIndex) {
	device.var[varIndex].valueAsInt = value
	device.var[varIndex].valueAsString = itoa(value)
	device.var[varIndex].type = AV_VAR_TYPE_INT
	return varIndex
    }
    
    for(varIndex = 1; varIndex <= MAX_LENGTH_ARRAY(device.var); varIndex ++) {
	if(device.var[varIndex].key == '') {
	    device.var[varIndex].key = varKey
	    device.var[varIndex].valueAsInt = value
	    device.var[varIndex].valueAsString = itoa(value)
	    device.var[varIndex].type = AV_VAR_TYPE_INT
	    return varIndex
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER AVDeviceVarSetString(_AV_DEVICE device, CHAR varKey[], CHAR value[]) {
    STACK_VAR INTEGER varIndex
    
    varIndex = AVDeviceVarIndexGet(device, varKey)
    
    if(AVDeviceVarKeyIsReservedWord(varKey)) {
	return 0
    }
    
    if(varIndex) {
	device.var[varIndex].valueAsInt = atoi(value)
	device.var[varIndex].valueAsString = value
	device.var[varIndex].type = AV_VAR_TYPE_CHAR
	return varIndex
    }
    
    for(varIndex = 1; varIndex <= MAX_LENGTH_ARRAY(device.var); varIndex ++) {
	if(device.var[varIndex].key == '') {
	    device.var[varIndex].key = varKey
	    device.var[varIndex].valueAsInt = atoi(value)
	    device.var[varIndex].valueAsString = value
	    device.var[varIndex].type = AV_VAR_TYPE_CHAR
	    return varIndex
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER AVDeviceVarTypeGet(_AV_DEVICE device, CHAR varKey[]) {
    STACK_VAR INTEGER varIndex
    
    varIndex = AVDeviceVarIndexGet(device, varKey)
    if(!varIndex) return 0
    
    return device.var[varIndex].type
}

DEFINE_FUNCTION INTEGER AVDeviceVarIsString(_AV_DEVICE device, CHAR varKey[]) {
    STACK_VAR INTEGER varIndex
    
    varIndex = AVDeviceVarIndexGet(device, varKey)
    if(!varIndex) return FALSE
    
    if(device.var[varIndex].type == AV_VAR_TYPE_CHAR) {
	return TRUE
    } else {
	return FALSE
    }
}

DEFINE_FUNCTION CHAR[AV_VAR_STR_MAX_LEN] AVDeviceVarGet(_AV_DEVICE device, CHAR varKey[]) {
    STACK_VAR INTEGER varIndex
    
    varIndex = AVDeviceVarIndexGet(device, varKey)
    if(!varIndex) return ''
    
    return device.var[varIndex].valueAsString
}

DEFINE_FUNCTION SINTEGER AVDeviceVarGetInt(_AV_DEVICE device, CHAR varKey[]) {
    STACK_VAR INTEGER varIndex
    
    varIndex = AVDeviceVarIndexGet(device, varKey)
    if(!varIndex) return 0
    
    return device.var[varIndex].valueAsInt
}
