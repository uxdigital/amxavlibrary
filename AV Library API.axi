PROGRAM_NAME='AV Library API'
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 09/24/2014  AT: 16:26:38        *)
(*******************************************************************************)
(*                                                                             *)
(*     _____            _              _  ____             _                   *)
(*    |     | ___  ___ | |_  ___  ___ | ||    \  ___  ___ |_| ___  ___  ___    *)
(*    |   --|| . ||   ||  _||  _|| . || ||  |  || -_||_ -|| || . ||   ||_ -|   *)
(*    |_____||___||_|_||_|  |_|  |___||_||____/ |___||___||_||_  ||_|_||___|   *)
(*                                                           |___|             *)
(*                      ___ _    __                                            *)
(*                     /   | |  / /                                            *)
(*                    / /| | | / /                                             *)
(*                   / ___ | |/ /                                              *)
(*                  /_/ _|_|__|/__                                             *)
(*                     / /   (_) /_  _________ ________  __                    *)
(*                    / /   / / __ \/ ___/ __ `/ ___/ / / /                    *)
(*                   / /___/ / /_/ / /  / /_/ / /  / /_/ /                     *)
(*                  /_____/_/_.___/_/   \__,_/_/   \__, /                      *)
(*                                                /____/                       *)
(*                                                                             *)
(*                   � Control Designs Software Ltd (2012)                     *)
(*                         www.controldesigns.co.uk                            *)
(*                                                                             *)
(*      Tel: +44 (0)1753 208 490     Email: support@controldesigns.co.uk       *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*                       AV Library API (Test Build)                           *)
(*                                                                             *)
(*            Written by Mike Jobson (Control Designs Software Ltd)            *)
(*                                                                             *)
(** REVISION HISTORY ***********************************************************)
(*                                                                             *)
(*  Please see README.md included with this library                            *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*  Permission is hereby granted, free of charge, to any person obtaining a    *)
(*  copy of this software and associated documentation files (the "Software"), *)
(*  to deal in the Software without restriction, including without limitation  *)
(*  the rights to use, copy, modify, merge, publish, distribute, sublicense,   *)
(*  and/or sell copies of the Software, and to permit persons to whom the      *)
(*  Software is furnished to do so, subject to the following conditions:       *)
(*                                                                             *)
(*  The above copyright notice and this permission notice and header shall     *)
(*  be included in all copies or substantial portions of the Software.         *)
(*                                                                             *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *)
(*  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *)
(*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *)
(*  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *)
(*  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT  *)
(*  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR   *)
(*  THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                 *)
(*                                                                             *)
(*******************************************************************************)

#DEFINE AVLIBRARY_CORE
#INCLUDE 'AV Library Core'

DEFINE_CONSTANT

#IF_NOT_DEFINED MAX_SOURCES
INTEGER MAX_SOURCES = 10
#END_IF

#IF_NOT_DEFINED MAX_DISPLAYS
INTEGER MAX_DISPLAYS = 4
#END_IF

#IF_NOT_DEFINED MAX_SPEAKERS
INTEGER MAX_SPEAKERS = 4
#END_IF

INTEGER AVTYPE_SOURCE = 1
INTEGER AVTYPE_DISPLAY = 2
INTEGER AVTYPE_SPEAKERS = 3

INTEGER AV_VAR_TYPE_INT = 1
INTEGER AV_VAR_TYPE_CHAR = 2

DEFINE_VARIABLE

_AV_DEVICE sources[MAX_SOURCES]
_AV_DEVICE displays[MAX_DISPLAYS]
_AV_DEVICE speakers[MAX_SPEAKERS]


/*
Use these functions in any code you implement this library
This enabled you to manage any source routing when you call a routing function

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>COPY BELOW<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

(*******************************************************************************)
(*  AV LIBRARY CALLBACK FUNCTIONS                                              *)
(*******************************************************************************)
DEFINE_FUNCTION AVDisplayShouldRoute(CHAR displayKey[], CHAR sourceKey[]) {
    STACK_VAR CHAR currentSourceKey[50]
    
    //Retreive current source key before routing.
    //New value is set automatically after this function completes.
    currentSourceKey = AVDisplayCurrentSource(displayKey)
    
    //Check if new source is nothing and power off if needed
    if(!LENGTH_STRING(sourceKey)) {
	
    }
    
    //Check if display is used and power up if needed
    else if(!LENGTH_STRING(currentSourceKey)) {
	
    }
    
    //Perform routing
    
}

DEFINE_FUNCTION AVSpeakersShouldRoute(CHAR speakersKey[], CHAR sourceKey[]) {
    STACK_VAR CHAR currentSourceKey[50]
    
    //Retreive current source key before routing.
    //New value is set automatically after this function completes.
    currentSourceKey = AVSpeakersCurrentSource(speakersKey)
    
    //If new source is a source and different then init volume if required
    if(LENGTH_STRING(sourceKey) && sourceKey <> currentSourceKey) {
	//Clear routing if current source to avoid weird level change
	if(LENGTH_STRING(currentSourceKey)) {
	    
	}
	
	//Init Volume
	
    }
    
    //Perform routing
    
}

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>COPY ABOVE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

*/

(*******************************************************************************)
(*                                                                             *)
(*  SOURCE API Methods                                                         *)
(*                                                                             *)
(*******************************************************************************)

(*******************************************************************************)
(*  AVSourcesInit()                                                            *)
(*******************************************************************************)
(*  Use to initialise all sources                                              *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     None                                                                    *)
(*  Returns:                                                                   *)
(*     None                                                                    *)
(*******************************************************************************)
DEFINE_FUNCTION AVSourcesInit() {
    AVDevicesInit(sources, AVTYPE_SOURCE)
}

(*******************************************************************************)
(*  AVSourceDefine(key, name)                                                  *)
(*******************************************************************************)
(*  Use to add a source or overwrite one with the same key                     *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key - The sources unique key value                               *)
(*     CHAR[] name - The sources printable name                                *)
(*  Returns:                                                                   *)
(*     INTEGER id - The resulting unique ID value of the source                *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourceDefine(CHAR key[], CHAR name[]) {
    return AVDeviceSet(sources, key, name)
}

(*******************************************************************************)
(*  AVSourceIDForKey(key)                                                      *)
(*******************************************************************************)
(*  Use to get the sources ID value from a given source key string             *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key - The sources unique key value                               *)
(*  Returns:                                                                   *)
(*     INTEGER id - The resulting unique ID value of the source                *)
(*                  Returns 0 if not found                                     *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourceIDForKey(CHAR key[]) {
    return AVDeviceIDForKey(sources, key)
}

(*******************************************************************************)
(*  AVSourceIndexForKey(key)                                                   *)
(*******************************************************************************)
(*  Use to get the sources index value from a given source key string          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key    - The sources unique key value                            *)
(*  Returns:                                                                   *)
(*     INTEGER index - The index of the source in the stored array             *)
(*                     Returns 0 if not found                                  *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourceIndexForKey(CHAR key[]) {
    return AVDeviceIndexForKey(sources, key)
}

(*******************************************************************************)
(*  AVSourceIndexForID(id)                                                     *)
(*******************************************************************************)
(*  Use to get the sources index value from a given source id                  *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     INTEGER id    - The sources unique id value                             *)
(*  Returns:                                                                   *)
(*     INTEGER index - The index of the source in the stored array             *)
(*                     Returns 0 if not found                                  *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourceIndexForID(INTEGER id) {
    return AVDeviceIndexForID(sources, id)
}

(*******************************************************************************)
(*  AVSourceIntPropertySet(sourceKey, key, value)                              *)
(*******************************************************************************)
(*  Use to assign an integer property to a source as a key / value pair        *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] sourceKey - The sources unique key value                         *)
(*     CHAR[] key       - The key of the property to assign to the source      *)
(*     SINTEGER value    - The value to assign to the property key             *)
(*  Returns:                                                                   *)
(*     INTEGER index    - The index of the var in the source var array         *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourceIntPropertySet(CHAR sourceKey[], CHAR key[], SINTEGER value) {
    STACK_VAR INTEGER sourceIndex
    
    sourceIndex = AVSourceIndexForKey(sourceKey)
    
    if(!sourceIndex) return 0
    
    return AVDeviceVarSetInt(sources[sourceIndex], key, value)
}

(*******************************************************************************)
(*  AVSourceStringPropertySet(sourceKey, key, value)                           *)
(*******************************************************************************)
(*  Use to assign a string property to a source as a key / value pair          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] sourceKey - The sources unique key value                         *)
(*     CHAR[] key       - The key of the property to assign to the source      *)
(*     CHAR[] value     - The value to assign to the property key              *)
(*  Returns:                                                                   *)
(*     INTEGER index    - The index of the var in the source var array         *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourceStringPropertySet(CHAR sourceKey[], CHAR key[], CHAR value[]) {
    STACK_VAR INTEGER sourceIndex
    
    sourceIndex = AVSourceIndexForKey(sourceKey)
    
    if(!sourceIndex) return 0
    
    return AVDeviceVarSetString(sources[sourceIndex], key, value)
}

(*******************************************************************************)
(*  AVSourceStringProperty(sourceKey, key)                                     *)
(*******************************************************************************)
(*  Use to get an string property from a source using a key                    *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] sourceKey - The sources unique key value                         *)
(*     CHAR[] key       - The key of the property to assign to the source      *)
(*  Returns:                                                                   *)
(*     CHAR[] value     - The value of the resulting property                  *)
(*                        Returns '' if error                                  *)
(*******************************************************************************)
DEFINE_FUNCTION CHAR[AV_VAR_STR_MAX_LEN] AVSourceStringProperty(CHAR sourceKey[], CHAR key[]) {
    STACK_VAR INTEGER sourceIndex
    
    sourceIndex = AVSourceIndexForKey(sourceKey)
    
    if(!sourceIndex) return 0
    
    return AVDeviceVarGet(sources[sourceIndex], key)
}

(*******************************************************************************)
(*  AVSourceIntProperty(sourceKey, key)                                        *)
(*******************************************************************************)
(*  Use to get an integer property from a source using a key                   *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] sourceKey - The sources unique key value                         *)
(*     CHAR[] key       - The key of the property to assign to the source      *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The value of the resulting property                  *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION SINTEGER AVSourceIntProperty(CHAR sourceKey[], CHAR key[]) {
    STACK_VAR INTEGER sourceIndex
    
    sourceIndex = AVSourceIndexForKey(sourceKey)
    
    if(!sourceIndex) return 0
    
    return AVDeviceVarGetInt(sources[sourceIndex], key)
}

(*******************************************************************************)
(*  AVSourcePropertyIsString(sourceKey, key)                                   *)
(*******************************************************************************)
(*  Use to establish if source property is a string type                       *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] sourceKey - The sources unique key value                         *)
(*     CHAR[] key       - The key of the property to assign to the source      *)
(*  Returns:                                                                   *)
(*     INTEGER value    - Return TRUE if property is char string               *)
(*                        Returns FALSE if error or not a char string          *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourcePropertyIsString(CHAR sourceKey[], CHAR key[]) {
    STACK_VAR INTEGER sourceIndex
    
    sourceIndex = AVSourceIndexForKey(sourceKey)
    
    if(!sourceIndex) return FALSE
    
    return AVDeviceVarIsString(sources[sourceIndex], key)
}

(*******************************************************************************)
(*  AVSourcesCount()                                                           *)
(*******************************************************************************)
(*  Use to get number of sources defined                                       *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     None                                                                    *)
(*  Returns:                                                                   *)
(*     INTEGER value    - Number of sources                                    *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSourcesCount() {
    return AVDevicesCount(sources)
}


(*******************************************************************************)
(*                                                                             *)
(*  DISPLAY API Methods                                                        *)
(*                                                                             *)
(*******************************************************************************)

(*******************************************************************************)
(*  AVDisplaysInit()                                                           *)
(*******************************************************************************)
(*  Use to initialise all displays                                             *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     None                                                                    *)
(*  Returns:                                                                   *)
(*     None                                                                    *)
(*******************************************************************************)
DEFINE_FUNCTION AVDisplaysInit() {
    STACK_VAR INTEGER n
    
    AVDevicesInit(displays, AVTYPE_SOURCE)
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(displays); n ++) {
	AVDeviceVarSetString(displays[n], 'currentSourceKey', '')
	AVDeviceVarSetInt(displays[n], 'currentSourceID', 0)
    }
}

(*******************************************************************************)
(*  AVDisplayDefine(key, name)                                                 *)
(*******************************************************************************)
(*  Use to add a display or overwrite one with the same key                    *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key - The displays unique key value                              *)
(*     CHAR[] name - The displays printable name                               *)
(*  Returns:                                                                   *)
(*     INTEGER id - The resulting unique ID value of the display               *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayDefine(CHAR key[], CHAR name[]) {
    return AVDeviceSet(displays, key, name)
}

(*******************************************************************************)
(*  AVDisplayIDForKey(key)                                                     *)
(*******************************************************************************)
(*  Use to get the displays ID value from a given display key string           *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key - The displays unique key value                              *)
(*  Returns:                                                                   *)
(*     INTEGER id - The resulting unique ID value of the display               *)
(*                  Returns 0 if not found                                     *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayIDForKey(CHAR key[]) {
    return AVDeviceIDForKey(displays, key)
}

(*******************************************************************************)
(*  AVDisplayIndexForKey(key)                                                  *)
(*******************************************************************************)
(*  Use to get the displays index value from a given display key string        *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key    - The displays unique key value                           *)
(*  Returns:                                                                   *)
(*     INTEGER index - The index of the display in the stored array            *)
(*                     Returns 0 if not found                                  *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayIndexForKey(CHAR key[]) {
    return AVDeviceIndexForKey(displays, key)
}

(*******************************************************************************)
(*  AVDisplayIndexForID(id)                                                    *)
(*******************************************************************************)
(*  Use to get the displays index value from a given display id                *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     INTEGER id    - The displays unique id value                            *)
(*  Returns:                                                                   *)
(*     INTEGER index - The index of the display in the stored array            *)
(*                     Returns 0 if not found                                  *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayIndexForID(INTEGER id) {
    return AVDeviceIndexForID(displays, id)
}

(*******************************************************************************)
(*  AVDisplayIntPropertySet(displayKey, key, value)                            *)
(*******************************************************************************)
(*  Use to assign an integer property to a display as a key / value pair       *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*     CHAR[] key       - The key of the property to assign to the display     *)
(*     SINTEGER value    - The value to assign to the property key             *)
(*  Returns:                                                                   *)
(*     INTEGER index    - The index of the var in the display var array        *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayIntPropertySet(CHAR displayKey[], CHAR key[], SINTEGER value) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    return AVDeviceVarSetInt(displays[displayIndex], key, value)
}

(*******************************************************************************)
(*  AVDisplayStringPropertySet(displayKey, key, value)                         *)
(*******************************************************************************)
(*  Use to assign a string property to a display as a key / value pair         *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*     CHAR[] key       - The key of the property to assign to the display     *)
(*     CHAR[] value     - The value to assign to the property key              *)
(*  Returns:                                                                   *)
(*     INTEGER index    - The index of the var in the display var array        *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayStringPropertySet(CHAR displayKey[], CHAR key[], CHAR value[]) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    return AVDeviceVarSetString(displays[displayIndex], key, value)
}

(*******************************************************************************)
(*  AVDisplayStringProperty(displayKey, key)                                   *)
(*******************************************************************************)
(*  Use to get an string property from a display using a key                   *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*     CHAR[] key       - The key of the property to assign to the display     *)
(*  Returns:                                                                   *)
(*     CHAR[] value     - The value of the resulting property                  *)
(*                        Returns '' if error                                  *)
(*******************************************************************************)
DEFINE_FUNCTION CHAR[AV_VAR_STR_MAX_LEN] AVDisplayStringProperty(CHAR displayKey[], CHAR key[]) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    return AVDeviceVarGet(displays[displayIndex], key)
}

(*******************************************************************************)
(*  AVDisplayIntProperty(displayKey, key)                                      *)
(*******************************************************************************)
(*  Use to get an integer property from a display using a key                  *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*     CHAR[] key       - The key of the property to assign to the display     *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The value of the resulting property                  *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION SINTEGER AVDisplayIntProperty(CHAR displayKey[], CHAR key[]) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    return AVDeviceVarGetInt(displays[displayIndex], key)
}

(*******************************************************************************)
(*  AVDisplayPropertyIsString(displayKey, key)                                 *)
(*******************************************************************************)
(*  Use to establish if display property is a string type                      *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*     CHAR[] key       - The key of the property to assign to the display     *)
(*  Returns:                                                                   *)
(*     INTEGER value    - Return TRUE if property is char string               *)
(*                        Returns FALSE if error or not a char string          *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplayPropertyIsString(CHAR displayKey[], CHAR key[]) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return FALSE
    
    return AVDeviceVarIsString(displays[displayIndex], key)
}

(*******************************************************************************)
(*  AVDisplaysCount()                                                          *)
(*******************************************************************************)
(*  Use to get number of displays defined                                      *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     None                                                                    *)
(*  Returns:                                                                   *)
(*     INTEGER value    - Number of displays                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplaysCount() {
    return AVDevicesCount(displays)
}

(*******************************************************************************)
(*  AVDisplayCurrentSource(displayKey)                                         *)
(*******************************************************************************)
(*  Use to current displayed source key from a display                         *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*  Returns:                                                                   *)
(*     CHAR[] value     - The key value of the current source                  *)
(*                        Returns '' if error                                  *)
(*******************************************************************************)
DEFINE_FUNCTION CHAR[AV_VAR_STR_MAX_LEN] AVDisplayCurrentSource(CHAR displayKey[]) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    return AVDeviceVarGet(displays[displayIndex], 'currentSourceKey')
}

(*******************************************************************************)
(*  AVDisplayCurrentSourceID(displayKey)                                       *)
(*******************************************************************************)
(*  Use to current displayed source ID from a display                          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The ID value of the current source                   *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION SINTEGER AVDisplayCurrentSourceID(CHAR displayKey[]) {
    STACK_VAR INTEGER displayIndex
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    return AVDeviceVarGetInt(displays[displayIndex], 'currentSourceID')
}

(*******************************************************************************)
(*  AVDisplaySourceSelect(displayKey, sourceKey)                               *)
(*******************************************************************************)
(*  Use to current displayed source ID from a display                          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] displayKey - The displays unique key value                       *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The ID value of the selected source                  *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVDisplaySourceSelect(CHAR displayKey[], CHAR sourceKey[]) {
    STACK_VAR INTEGER displayIndex
    STACK_VAR INTEGER sourceID
    
    displayIndex = AVDisplayIndexForKey(displayKey)
    
    if(!displayIndex) return 0
    
    sourceID = AVSourceIDForKey(sourceKey)
    
    if(!sourceID) return 0
    
    AVDisplayShouldRoute(displayKey, sourceKey)
    
    AVDeviceVarSetInt(displays[displayIndex], 'currentSourceID', TYPE_CAST(sourceID))
    AVDeviceVarSetString(displays[displayIndex], 'currentSourceKey', sourceKey)
    
    return sourceID
}


(*******************************************************************************)
(*                                                                             *)
(*  SPEAKERS API Methods                                                        *)
(*                                                                             *)
(*******************************************************************************)

(*******************************************************************************)
(*  AVSpeakersInit()                                                           *)
(*******************************************************************************)
(*  Use to initialise all speakers                                             *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     None                                                                    *)
(*  Returns:                                                                   *)
(*     None                                                                    *)
(*******************************************************************************)
DEFINE_FUNCTION AVSpeakersInit() {
    STACK_VAR INTEGER n
    
    AVDevicesInit(speakers, AVTYPE_SOURCE)
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(speakers); n ++) {
	AVDeviceVarSetString(speakers[n], 'currentSourceKey', '')
	AVDeviceVarSetInt(speakers[n], 'currentSourceID', 0)
	AVDeviceVarSetInt(speakers[n], 'currentVolume', 0)
    }
}

(*******************************************************************************)
(*  AVSpeakersDefine(key, name)                                                *)
(*******************************************************************************)
(*  Use to add a speakers or overwrite one with the same key                    *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key - The speakers unique key value                              *)
(*     CHAR[] name - The speakers printable name                               *)
(*  Returns:                                                                   *)
(*     INTEGER id - The resulting unique ID value of the speakers               *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersDefine(CHAR key[], CHAR name[]) {
    return AVDeviceSet(speakers, key, name)
}

(*******************************************************************************)
(*  AVSpeakersIDForKey(key)                                                     *)
(*******************************************************************************)
(*  Use to get the speakers ID value from a given speakers key string           *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key - The speakers unique key value                              *)
(*  Returns:                                                                   *)
(*     INTEGER id - The resulting unique ID value of the speakers               *)
(*                  Returns 0 if not found                                     *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersIDForKey(CHAR key[]) {
    return AVDeviceIDForKey(speakers, key)
}

(*******************************************************************************)
(*  AVSpeakersIndexForKey(key)                                                 *)
(*******************************************************************************)
(*  Use to get the speakers index value from a given speakers key string       *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] key    - The speakers unique key value                           *)
(*  Returns:                                                                   *)
(*     INTEGER index - The index of the speakers in the stored array           *)
(*                     Returns 0 if not found                                  *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersIndexForKey(CHAR key[]) {
    return AVDeviceIndexForKey(speakers, key)
}

(*******************************************************************************)
(*  AVSpeakersIndexForID(id)                                                   *)
(*******************************************************************************)
(*  Use to get the speakers index value from a given speakers id               *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     INTEGER id    - The speakers unique id value                            *)
(*  Returns:                                                                   *)
(*     INTEGER index - The index of the speakers in the stored array           *)
(*                     Returns 0 if not found                                  *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersIndexForID(INTEGER id) {
    return AVDeviceIndexForID(speakers, id)
}

(*******************************************************************************)
(*  AVSpeakersIntPropertySet(speakersKey, key, value)                          *)
(*******************************************************************************)
(*  Use to assign an integer property to a speakers as a key / value pair      *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*     CHAR[] key       - The key of the property to assign to the speakers    *)
(*     SINTEGER value    - The value to assign to the property key             *)
(*  Returns:                                                                   *)
(*     INTEGER index    - The index of the var in the speakers var array       *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersIntPropertySet(CHAR speakersKey[], CHAR key[], SINTEGER value) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarSetInt(speakers[speakersIndex], key, value)
}

(*******************************************************************************)
(*  AVSpeakersStringPropertySet(speakersKey, key, value)                       *)
(*******************************************************************************)
(*  Use to assign a string property to a speakers as a key / value pair        *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*     CHAR[] key       - The key of the property to assign to the speakers    *)
(*     CHAR[] value     - The value to assign to the property key              *)
(*  Returns:                                                                   *)
(*     INTEGER index    - The index of the var in the speakers var array       *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersStringPropertySet(CHAR speakersKey[], CHAR key[], CHAR value[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarSetString(speakers[speakersIndex], key, value)
}

(*******************************************************************************)
(*  AVSpeakersStringProperty(speakersKey, key)                                 *)
(*******************************************************************************)
(*  Use to get an string property from a speakers using a key                  *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*     CHAR[] key       - The key of the property to assign to the speakers    *)
(*  Returns:                                                                   *)
(*     CHAR[] value     - The value of the resulting property                  *)
(*                        Returns '' if error                                  *)
(*******************************************************************************)
DEFINE_FUNCTION CHAR[AV_VAR_STR_MAX_LEN] AVSpeakersStringProperty(CHAR speakersKey[], CHAR key[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarGet(speakers[speakersIndex], key)
}

(*******************************************************************************)
(*  AVSpeakersIntProperty(speakersKey, key)                                    *)
(*******************************************************************************)
(*  Use to get an integer property from a speakers using a key                 *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*     CHAR[] key       - The key of the property to assign to the speakers    *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The value of the resulting property                  *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION SINTEGER AVSpeakersIntProperty(CHAR speakersKey[], CHAR key[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarGetInt(speakers[speakersIndex], key)
}

(*******************************************************************************)
(*  AVSpeakersPropertyIsString(speakersKey, key)                               *)
(*******************************************************************************)
(*  Use to establish if speakers property is a string type                     *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*     CHAR[] key       - The key of the property to assign to the speakers    *)
(*  Returns:                                                                   *)
(*     INTEGER value    - Return TRUE if property is char string               *)
(*                        Returns FALSE if error or not a char string          *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersPropertyIsString(CHAR speakersKey[], CHAR key[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return FALSE
    
    return AVDeviceVarIsString(speakers[speakersIndex], key)
}

(*******************************************************************************)
(*  AVSpeakersCount()                                                          *)
(*******************************************************************************)
(*  Use to get number of speakers defined                                      *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     None                                                                    *)
(*  Returns:                                                                   *)
(*     INTEGER value    - Number of speakers                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersCount() {
    return AVDevicesCount(speakers)
}

(*******************************************************************************)
(*  AVSpeakersCurrentSource(speakersKey)                                       *)
(*******************************************************************************)
(*  Use to current selected source key from a speakers                         *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*  Returns:                                                                   *)
(*     CHAR[] value     - The key value of the current source                  *)
(*                        Returns '' if error                                  *)
(*******************************************************************************)
DEFINE_FUNCTION CHAR[AV_VAR_STR_MAX_LEN] AVSpeakersCurrentSource(CHAR speakersKey[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarGet(speakers[speakersIndex], 'currentSourceKey')
}

(*******************************************************************************)
(*  AVSpeakersCurrentSourceID(speakersKey)                                     *)
(*******************************************************************************)
(*  Use to current selected source ID from a speakers                          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The ID value of the current source                   *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION SINTEGER AVSpeakersCurrentSourceID(CHAR speakersKey[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarGetInt(speakers[speakersIndex], 'currentSourceID')
}

(*******************************************************************************)
(*  AVSpeakersVolume(speakersKey)                                              *)
(*******************************************************************************)
(*  Use to current selected source ID from a speakers                          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakers unique key value                      *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The ID value of the current source                   *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION SINTEGER AVSpeakersVolume(CHAR speakersKey[]) {
    STACK_VAR INTEGER speakersIndex
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    return AVDeviceVarGetInt(speakers[speakersIndex], 'currentSourceID')
}

(*******************************************************************************)
(*  AVSpeakersSourceSelect(speakersKey, sourceKey)                               *)
(*******************************************************************************)
(*  Use to current speakersed source ID from a speakers                          *)
(*******************************************************************************)
(*  Arguments:                                                                 *)
(*     CHAR[] speakersKey - The speakerss unique key value                       *)
(*  Returns:                                                                   *)
(*     INTEGER value    - The ID value of the selected source                  *)
(*                        Returns 0 if error                                   *)
(*******************************************************************************)
DEFINE_FUNCTION INTEGER AVSpeakersSourceSelect(CHAR speakersKey[], CHAR sourceKey[]) {
    STACK_VAR INTEGER speakersIndex
    STACK_VAR INTEGER sourceID
    
    speakersIndex = AVSpeakersIndexForKey(speakersKey)
    
    if(!speakersIndex) return 0
    
    sourceID = AVSourceIDForKey(sourceKey)
    
    if(!sourceID) return 0
    
    AVSpeakersShouldRoute(speakersKey, sourceKey)
    
    AVDeviceVarSetInt(speakers[speakersIndex], 'currentSourceID', TYPE_CAST(sourceID))
    AVDeviceVarSetString(speakers[speakersIndex], 'currentSourceKey', sourceKey)
    
    return sourceID
}